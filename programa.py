###########################################################################################################################################################
#   DESAFIO ESTÁGIO DRIVA
# ---------------------------------------------------------------------------------------------------------------------------------------------------------
# Autor: Thiago Alves Oliveira
# Versão: 1.0
# Data: 18/11/2021
#
###########################################################################################################################################################
import pandas as pd
import matplotlib.pyplot as plt
from pandas.core.frame import DataFrame

###########################################################################################################################################################
# LÊ O ARQUIVO
PATH = r'DadosEmpresa.csv'
PATH2 = r'DadosEndereco.csv'

empresa = pd.read_csv(PATH)
endereco = pd.read_csv(PATH2)

###########################################################################################################################################################
def empresasSIM(tam, j):
    df = DataFrame(columns=['cnpj','raiz_cnpj','matriz_filial','razao_social','opcao_pelo_simples','capital_social'])
    for i in range(0, tam):
        if empresa.loc[i, 'opcao_pelo_simples'] == 'SIM':
            df.loc[j] = empresa.loc[i]
            j+=1
    
    df.to_csv('empresasSIM.csv')

###########################################################################################################################################################
def empresas5000(tam, j):
    df = DataFrame(columns=['cnpj','raiz_cnpj','matriz_filial','razao_social','opcao_pelo_simples','capital_social'])
    for i in range(0, tam):
        if (endereco.loc[i, 'municipio'] == 'CURITIBA' or endereco.loc[i, 'municipio'] == 'CURITIBA') and (empresa.loc[i, 'capital_social'] > 5000):
            df.loc[j] = empresa.loc[i]
            j+=1

    df.to_csv('empresas5000.csv')

###########################################################################################################################################################
def grafc1(tam, j):
    df = DataFrame(columns=['cnpj','bairro','municipio'])
    for i in range(0, tam):
        if endereco.loc[i, 'municipio'] == 'CURITIBA':
            df.loc[j] = endereco.loc[i]
            j+=1

    data_df = df.groupby(['bairro']).agg(count=('bairro', 'count'))
    data_df = data_df.reset_index() 
    data_df = data_df.sort_values(['count'])
    plt.bar(data_df['bairro'], data_df['count'])
    plt.show()

###########################################################################################################################################################
def solv(tam, j):
    df = DataFrame(columns=['cnpj','municipio','bairro','capital_social','razao_social','opcao_pelo_simples'])
    j = 0
    for i in range(0, tam):
        if endereco.loc[i, 'municipio'] == 'CURITIBA':
            df.loc[j,'cnpj'] = endereco.loc[i,'cnpj']
            df.loc[j,'municipio'] = endereco.loc[i,'municipio']
            df.loc[j,'bairro'] = endereco.loc[i,'bairro']
            df.loc[j,'capital_social'] = empresa.loc[i,'capital_social']
            df.loc[j,'razao_social'] = empresa.loc[i,'razao_social']
            df.loc[j,'opcao_pelo_simples'] = empresa.loc[i,'opcao_pelo_simples']
            j+=1

    data_df = df.groupby(['bairro']).agg(sum=('capital_social', 'sum'))
    data_df = data_df.reset_index()
    data_df = data_df.sort_values(['sum'])
    plt.bar(data_df['bairro'], data_df['sum'])
    plt.show()

###########################################################################################################################################################
# MAIN
def main():
    tam = int(len(empresa))

    ###########################################################################################################################################################
    # Gere um arquivo csv com todas as empresas que tem opção_pelo_simples como SIM;
    empresasSIM(tam, j=0)

    ###########################################################################################################################################################
    # Gere outro arquivo csv que contenha todas as informações das empresas que são de Curitiba ou de Londrina e que tenham capital social maior que 5000 reais.    
    empresas5000(tam, j=0)

    ###########################################################################################################################################################
    # Faça um gráfico que mostre o total de empresas em cada bairro de Curitiba. (utilize uma biblioteca de sua escolha);
    grafc1(tam, j=0)

    ###########################################################################################################################################################
    # Observando os dois arquivos, qual outra analise de dados você faria? Implemente ela e escreva em um comentário o porque pensou nela.
    #
    # RESPOSTA: 
    #
    # Um gráfico com os bairros que possuem maior capital social acumulado
    # Descobrimos os bairros que possuem as empresas mais robustas, que precisam de mais dinheiro para gerar receita
    solv(tam, j=0)

###########################################################################################################################################################
if __name__ == '__main__':
    main()
###########################################################################################################################################################